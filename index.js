var express = require("express");
var bodyParser = require("body-parser");
var webPush = require("web-push");

/**
 * @typedef {Object} Body
 * @prop {Notification} notification
 * @prop {Array<Subscriptor} subscriptors
 *
 * @typedef {Object} Notification
 * @prop {string} action
 *
 * @typedef {Object} Subscriptor
 * @prop {string} token
 * @prop {string} p256dh
 * @prop {string} auth
 */

var app = express();
var hostname = "localhost";
var port = "8888";
var errors = [];
var response = [];

// To take post parameters
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

/**
 * Setup VAPID keys
 * @param {Object} keys
 * @param {string} keys.public
 * @param {string} keys.auth
 */
function setupKeys(keys) {
  webPush.setVapidDetails("https://www.man-app.com", keys.public, keys.private);
}

/**
 * Loop through all subscriptors and send them a push notification
 * @param {Notification} notification
 * @param {Array<Subscriptor>} subscriptors
 * @return {Promise}
 */
function sendNotifications(notification, subscriptors) {
  return Promise.all([
    subscriptors
      .map(function(subscriptor) {
        return {
          endpoint: subscriptor.token,
          keys: {
            p256dh: subscriptor.p256dh,
            auth: subscriptor.auth
          }
        };
      })
      .forEach(function(subscriptor) {
        return webPush
          .sendNotification(subscriptor, JSON.stringify(notification))
          .catch(function(err) {
            console.log("A promise failed");
            errors.push(err);
            return err;
          });
      })
  ]);
}

function handleRequest(req, res) {
  /**
   * Parsed request body
   * @type {Body}
   */
  const body = JSON.parse(Object.keys(req.body)[0]);

  if (!body.keys) {
    return res.send("No keys have been sent");
  }

  if (!body.keys.public) {
    return res.send("No public key has been sent");
  }

  if (!body.keys.private) {
    return res.send("No private key has been sent");
  }

  if (!body.subscriptors || !body.subscriptors.length) {
    return res.send("There are no subscriptors to be notified");
  }

  if (!body.notification) {
    return res.send("There is not any notification to be sent");
  }

  setupKeys(body.keys);

  sendNotifications(body.notification, body.subscriptors).then(function() {
    if (errors.length) {
      console.log("There were errors while sending notifications", errors);
      errors = [];
      return res.send(response);
    }

    console.log("All notifications have been sent");
    errors = [];
    return res.send(response);
  });
}

app.get("/", function(req, res) {
  res.send("Pusher is working");
});

app.post("/", handleRequest);

app.listen(port, hostname, function() {
  console.log("Listening on " + hostname + ":" + port);
});
